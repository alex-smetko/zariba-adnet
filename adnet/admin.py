from django.contrib import admin
from adnet.models import Ad, Adowner, Apikey

# Register your models here.

class AdInline(admin.StackedInline):
	model = Ad
	extra = 1
	fields = ['url','banner']

class AdownerAdmin(admin.ModelAdmin):
	inlines = [AdInline]
	fields = ['name','percent']
	list_display = ['name','percent','impressions','error_deviation']
	list_filter = ['name']
	search_fields = ['name','impressions']

class AdAdmin(admin.ModelAdmin):
	list_display = ['adowner_name','impressions','url']
	fields = ['url','banner']

class ApikeyAdmin(admin.ModelAdmin):
	fields =['keyhash']
	list_display = ['keyhash']


admin.site.register(Adowner, AdownerAdmin)
admin.site.register(Ad,AdAdmin)
admin.site.register(Apikey,ApikeyAdmin)

# admin.site.register(Adowner)

