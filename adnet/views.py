from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
# from django.template import RequestContext, loader
import json
import datetime as dt
import base64

from adnet.models import Apikey, Ad, Adowner


def index(request):
	return HttpResponse(render(request, "adnet/index.html", {}))

def get_ad(request, keyhash):
	"""
	This is the inteded REST method to be called like so:
	http://domain:port/adnet/get_ad/<apikey>
	For the purpose of this demo you'll probably want to visit:
	http://localhost:8000/adnet/get_ad/0b92d81a5ea82b19879712f5019fd8eae68ee3d8

	Returns JSON object which always contains a "status" key (either OK or ERROR).
	If status is ERROR, there'll be a error_deviationr and error_code keys explaining
	the error
	"""

	data = {"status" : "OK"}
	try:
		kh = Apikey.objects.get(keyhash=keyhash)
		try:
			adowner = Adowner.objects.order_by('-error_deviation', '-percent')[0]
			try:
				ad = adowner.serve_ad()
				data['url'] = ad.url
				# data['img'] = ad.banner.path
				try:
					data['img'] = base64.b64encode(open(ad.banner.path,'rb').read())
				except Exception, e:
					data['status'] = "ERROR"
					data['error_code'] = "500"
					data['error_message'] = "Could not open file %s" % ad.banner.name
			except Exception, e:
				data['status'] = "ERROR"
				data['error_message'] = "Could not get ad from %s" % adowner
				data['error_code'] = '500'
		except Exception, e:
			data['status'] = "ERROR"
			data['error_message'] = "Could not get a Adowner: %s" % e
			data['error_code'] = '500'
	except Exception, e:
		data['status'] = "ERROR"
		data['error_code'] = '401'
		data['error_message'] = "Invalid or empty API key %s" % keyhash


	data = json.dumps(data,indent=4)
	return HttpResponse(data,content_type="text/json")