from django.conf.urls import patterns, url

from adnet import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^get_ad/(?P<keyhash>.*)$', views.get_ad, name='get_ad')
)