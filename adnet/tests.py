from django.test import TestCase
from adnet.models import Adowner,Ad

import os

# Create your tests here.
class AdownerTestCase(TestCase):
	fixtures = ['test-data.adnet.json']
	
	def test_serve_ad(self):
		conf_path = os.path.dirname(__file__)+"/conf.json"
		self.assertTrue(os.path.exists(conf_path))
