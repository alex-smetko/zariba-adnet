from django.db import models, connection
from django.db.models import F

from django.db.models.signals import post_save
from django.dispatch import receiver

import hashlib
import datetime as dt
import random
import os
import json
# Create your models here.

def generateApikey():
	"""Generates a random hash to be used as API key"""

	seed = str(dt.datetime.now())+str(random.random())
	return hashlib.sha1(seed).hexdigest()

class Adowner(models.Model):
	name = models.CharField(max_length=32)
	impressions = models.IntegerField(default=0) # total impressions for this advertiser
	percent = models.FloatField() # how often ads of this owner should be showed in percent
	current_percent = models.FloatField(default=0) # actual percent of showed ads
	error_deviation = models.FloatField(default=100) # current_percent/percent as percentage

	def __str__(self):
		return self.name

	def serve_ad(self):
		"""
		Returns a dict with url and base64 encoded image for the next ad for this advertiser to 
		be shown and reduces the error_deviation attribute
		"""
		conf_path = os.path.dirname(__file__)+"/conf.json"
		try:
			with open(conf_path, "r+") as conf_file:
				conf = json.load(conf_file)
				ti = conf['total_impressions'] = conf['total_impressions']+1
				if ti < 100: ti = 100

				self.impressions += 1
				self.save()
				
				# TODO try/catch exception when there are no ads
				ad = self.ad_set.order_by('impressions')[0] # ad with least impressions
				ad.impressions += 1
				ad.save()

				# because for some reason I couldn't make it in bulk with .update()
				for o in Adowner.objects.all():
					o.current_percent = F('impressions')/((ti/100))
					o.error_deviation = 100-F('current_percent')/(F('percent')/100)
					o.save()

				# overwrite total impressions
				conf_file.seek(0)
				conf_file.write(json.dumps(conf,indent=4))
				conf_file.truncate()
				

				return ad
		except IOError, e:
			conf = {"total_impressions":0}
			with open(conf_path,"w") as conf_file: conf_file.write(json.dumps(conf,indent=4))

		return None

class Ad(models.Model):
	"""
	Represents a single advertisement, belongs to Adowner.
	Each time a Adowner is chosed to show an Ad it shows the
	one with least impressions
	"""
	url = models.CharField(max_length=256,default="http://zariba.com")
	banner = models.FileField(upload_to='adbaners')
	impressions = models.IntegerField(default=0) # impressions for this ad only
	adowner = models.ForeignKey(Adowner)

	def __str__(self):
		return self.adowner.name+" "+self.url

	def adowner_name(self):
		return self.adowner.name

class Apikey(models.Model):
	"""
	Needed to make the call like so:
	http://domain.tld:port/adnet/get_ad/<your_api_key_here>
	"""
	keyhash = models.CharField(max_length=40, default=generateApikey() )

	def __str__(self):
		return self.keyhash


class Adrequest(models.Model):
	"""
	Not implemented. Also not appropriate for relational databases
	as it would grow too fast. Better use syslog or other file based
	logging system
	"""
	adowner = models.ForeignKey(Adowner) # Adowner for the returned Ad
	ad = models.ForeignKey(Ad) # returned Ad

@receiver(post_save, sender=Adowner)
def reset_adowners(sender, instance, signal, created, **kwargs):
	"""
	If a new advertiser is created, deduct others' show percent
	to open up space for this one. Also reset total impressions
	as it might take ages for this Adowner to catch up with the
	other Adowner impressions. Intentionally don't reset ad 
	impressions as it doesn't have any impact on the end result
	"""

	if created:
		Adowner.objects.exclude(id=instance.id).update(percent=F('percent')*(1-instance.percent/100))
		Adowner.objects.update(impressions=0,error_deviation=100)
		with open(os.path.dirname(__file__)+"/conf.json", "r+") as conf_file:
			conf = json.load(conf_file)
			conf['total_impressions'] = 0
			conf_file.seek(0)
			conf_file.write(json.dumps(conf,indent=4))
			conf_file.truncate()