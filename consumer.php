<?php

$apikey = "0b92d81a5ea82b19879712f5019fd8eae68ee3d8"; //default
$outfile = dirname(__FILE__)."/out.txt";
$number_of_calls = 1000;

for ($i=0; $i < $number_of_calls; $i++) { 
	$out = "";
	$data = json_decode(file_get_contents("http://localhost:8000/adnet/get_ad/".$apikey),true);
	if ($i == 0) {
		$out = implode("\t", array_keys($data))."\n#$i ";
	}
	if(isset($data['img'])) $data['img'] = substr($data['img'], 220,20);
	$out .= implode("\t", array_values($data))."\n";
	if ($i != 0) $out = "#$i ".$out;

	if(file_exists($outfile)) {
		file_put_contents($outfile, $out, FILE_APPEND);
	} else {
		file_put_contents($outfile, $out);
	}
	
	echo $out;
}
?>