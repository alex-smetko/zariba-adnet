# Overview

This is a test app as described in Bulgarian [here](https://docs.google.com/document/d/1N7c1ul144_OuKlvqHsSdVn6BdafOo43vwMflQEQn75k/edit?pli=1#)

# Installation

You will need:

## Python 2.7.X

Ofcourse

* [Windows](http://python.org/ftp/python/2.7.6/python-2.7.6.msi)
* Linux - you should already have it. If you also have Python 3, use `/usr/bin/python2.7`

## PIP

`pip` is a package manager for Python. It will easily install latest version of Django for you. To install it download the [get-pip.py](https://raw.github.com/pypa/pip/master/contrib/get-pip.py) file and run it `python get-pip.py`

## Django 1.6

This is the easy part. Just run
```
pip install django
```
and it will set up all the dependancies. You are now clear to proceed with the demo app itself. Clone the app wherever you find suitable.

# Running the adnet

For the purpose of this app, it's built over sqlite database which is included in the repo and working outside of the box. Just navigate to the project root and run the built-in server `python manage.py runserver` and visit [http://localhost:8000/admin](http://localhost:8000/admin). Username is `admin` and password is `1234`. You don't need to alter anything at this moment, just take a look around.

Adding a new advertiser is done by adding a new Adowner entity along with at least one Ad for him and the desired integer percent for the frequency the ads will be showed at (e.g. `50` means `50%`). This will automatically deduct other advertisers show percent for the new Adowner to fit in. This will also wipe out any existing data for past impressions to give a clean start.

## Note on adding Adowners

If you happen to clear out all the existing advertisers and you're adding a new one, make sure you type `100` for percent. For any further adowners added just type in the desired ad showing frequency percent. E.g. if you were to add Alice and Bob adowners in a empty database, you should first add Alice as 100%, then add Bob with 30% which will automatically deduct Alice down to 70%l

# Getting ads

You can run the consumer PHP script as CLI: `php consumer.php`. This will also create a `out.txt.` file with all received JSON responses as tab delimited values (you can copy/paste this into Excel or other spreadsheet to analyze). `out.txt` already exists, so you can directly view the output of the service. Base64 encoded images will be truncated for brevity.

Alternatively, you can check the output yourself by visiting [http://localhost:8000/adnet/get_ad/0b92d81a5ea82b19879712f5019fd8eae68ee3d8](http://localhost:8000/adnet/get_ad/0b92d81a5ea82b19879712f5019fd8eae68ee3d8) which is a REST API call with the default API key as a GET parameter.

# Refreshing the database

There's also a backup of the database and a script to overwrite a config file along with it. Just make it executable by running `chmod +x reset.sh` and execute it while in the project root `./reset.sh`. This will also remove the `out.txt` file by default containing the output of thousand API calls.

# Algorithms explained

## Fair show percent deduction when adding new advertisers

Instead of PID algorithm the implementation simply reduces every existing advertisers percent by the same percentage so when it sums it's below 100% and there's space for the new advertisers. E.g. if we want to add a new advertiser having 2% ad showing frequency, when we add him in the admin panel other advertisers frequency percent is recalculated `0.98 * old desired frequency percent`. Because operation is done to all existing adowners, their desired frequency percent sums up to 98% leaving a span wide just enough for the new 2% adowner.

## Percent based advertisement frequency distribution

This is really close to the desired result (as results, not implementation). The app keeps track of each adowners error_deviation which is really a `current frequency / desired frequency` stored as percent or at least that was the intdended result. Each ad to be serverd is picked to be the first upon it's least impressions and it's advertiser having the biggest error deviation and biggest desired frequency as a second sort key.